# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name "Finn"
    email "finn@candykingdom.com.oo"
  end
end
