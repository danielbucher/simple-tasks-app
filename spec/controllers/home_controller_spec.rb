require 'spec_helper'

describe HomeController, :type => :controller do
  describe 'GET #index' do
    before { get :index }

    it { should render_template('index') }
  end
end
